# Sperrungen föderierter Nutzer

| Nutzername                                   | Art der Sperrung          | Grund der Sperrung                              | Meldung         | Sanktion        |
| -------------------------------------------- | ------------------------- | ----------------------------------------------- | --------------- | --------------- |
| `@submedia@mastodon.social`                  | Stummschaltung            | Verbreitung von extremistischem Gedankengut     | 19.03.19, 20:22 | 20.03.19, 11:22 |
| `@semibot@greenlifeplus.net`                 | Sperrung und Löschung     | Botspam in der föderierten Zeitleiste, Stalking |                 | 26.03.19, 12:14 |
| `@federationbot@mastodon.host`               | Sperrung und Löschung     | Botspam in der föderierten Zeitleiste, Stalking |                 | 26.03.19, 12:13 |
| `@Wnt_2b_crusader@social.privacytools.io`    | Stummschaltung            | Homophobe Äußerungen                            | 09.04.19, 07:08 | 15.04.19, 11:19 |
| `@OmnaBrain@anticapitalist.party`            | Stummschaltung            | Gewaltaufruf gegen Menschengruppen              | 30.04.19, 07:06 | 30.04.19, 07:06 |
| `@NecroTechno@mastodon.social`               | Stummschaltung            | Hass gegen Menschengruppen                      | 22.04.19, 16:49 | 22.04.19, 23:27 |
| `@GreenandBlack@sunbeam.city`                | Stummschaltung            | Hass und Gewaltaufruf gegen Menschengruppen     | 17.04.19, 11:41 | 17.04.19, 11:42 |
| `@Data@ruhr.social`                          | Stummschaltung            | Verbreitung von Verschwöhrungstheorien          | 05.06.19, 15:53 | 10.06.19, 14:17 |
| `@Kimberly98@social.privacytools.io`         | Sperrung und Löschung | Spam, Hass gegen Menschengruppen                | 15.06.19, 13:43 | 15.06.19, 13:43 |
| `@PresidentTrumpAwesome@mastodon.technology` | Sperrung und Löschung | Spam                                            | 17.06.19, 19:12 | 17.06.19, 19:25 |
| `@BabyBitch8@im-in.space`                    | Sperrung und Löschung | Spam                                            | 17.06.19, 19:14 | 17.06.19, 19:14 |
|`savi001@soc.hardwarepunk.de`|Sperrung und Löschung|Spam|24.06.19, 12:23|24.06.19, 12:24|
|`Jennifer8@metalhead.club`|Sperrung und Löschung|Spam|28.06.19, 21:28|28.06.19, 21:31|
|`GoodGoing5423@mastodon.cloud`|Sperrung und Löschung|Spam|01.07.19, 12:44|01.07.19, 12:47|
|`TransLesbianLily@mastodon.social`| Stummschaltung|Gewaltaufruf gegen Menschengruppen|05.07.19, 09:07|05.07.19, 10:04|
|`Liechenstein@mstdn.su`|Sperrung und Löschung|Verbreitung von Verschwöhrungstheorien|13.07.19, 01:36|13.07.19, 12:21|
|`Paulaliechenstein@mastodon.xyz`|Sperrung und Löschung|Verbreitung von Verschwöhrungstheorien|13.07.19, 23:42|14.07.19, 08:43|
|`NGHix@mastodon.cloud`|Sperrung und Löschung|Verbreitung von Verschwöhrungstheorien|16.07.19, 11:42|16.07.19, 12:39|
| `@privacyint@mastodon.xyz`                   | Stummschaltung            | Spam, fehlerhaft konfigurrierter Crossposter    | 26.07.19, 06:17 | 26.07.19, 06:55 |


# Sperrung eigener Nutzer

| Nutzername          | Art der Sperrung      | Grund der Sperrung                                           | Meldung          | Sanktion        |
| ------------------- | --------------------- | ------------------------------------------------------------ | ---------------- | --------------- |
| `@Reiseberich`      | Sperrung und Löschung | Verbreitung von Verschwöhrungstheorien und Rechtsextremismus, Doxxing | 17.02.19, 17:47 | 18.02.19, 00:00 |
| `@TopEssayServices` | Sperrung und Löschung | Dringlicher Verdacht auf Spam-Absichten                      |                  | 19.06.19, 16:08 |
| `@papercoach` | Sperrung und Löschung | Dringlicher Verdacht auf Spam-Absichten                      |                  | 19.06.19, 19:31 |
| `@jjjnordis99`      | Sperrung und Löschung | Dringlicher Verdacht auf Spam-Absichten                      |                  | 19.06.19, 19:52 |
|`@packersandmoversindore`|Sperrung und Löschung|Dringlicher Verdacht auf Spam-Absichten||29.06.19, 11:28|
|`@papercoach`|Sperrung und Löschung|Dringlicher Verdacht auf Spam-Absichten||21.06.19, 19:31|


# Instanz-Sperrungen
Instanzsperrungen sind [hier](https://machteburch.social/about/more#unavailable-content) zu finden.

# Kontakt | *Contact us*

Solltest Du in dieser Liste auftauchen und der Meinung sein, zu Unrecht sanktioniert worden zu sein, kontaktiere uns gerne. Unsere Kontaktmöglichkeiten findest Du [hier](https://machteburch.social/about/more).

*If you appear on this list and feel that you have been wrongly sanctioned, please contact us. You can find our contact details [here](https://machteburch.social/about/more).*
