Hier mal eine kleine Empfehlungsstrecke zur Nutzung von Mastodon:

## Apps:

### Android: 

[Tusky](https://tuskyapp.github.io/)

### iOS: 

- [Toot!](https://itunes.apple.com/app/toot/id1229021451) kostenpflichtig, soll aber ganz gut sein. Open Source, das Geld fließt in die Entwicklung 
- [Amaroq](https://itunes.apple.com/us/app/amaroq-for-mastodon/id1214116200) kostenlose Alternative

### Weitere: 

[hier](https://joinmastodon.org/apps)

## Twitter:

### Accounts synchronisieren

*Bitte bei der Nutzung unsere Regeln beachten: ungelistet posten*

[moa.party](https://moa.party/)

### Interaktion mit anderen Netzwerken

Ermöglicht es, in ActivityPub-Netzwerken Nutzer von anderen Netzwerken zu erwähnen

[activitypub.actor](https://activitypub.actor/)

### Twitter-Freunde auf Mastodon finden

[hier](https://bridge.joinmastodon.org/)

## Außerdem:

[Digitale Amnesie für Mastodon und Twitter](https://forget.codl.fr/about/)

## Interessante Instanzen:

- [botsin.space](https://botsin.space): Eine reine Bot-Instanz. (Bots sind ein offizielles Feature von Mastodon.) Hier gibts alles zwischen "ziemlich cool" und "wtf? O_o"
- [newsbots.eu](https://newsbots.eu): Eine Instanz nur für News-Crossposts. 

### Interessante nicht-instanz Seiten:

- [Fediverse Party](https://fediverse.party/): Eine Übersichtsseite zum Fediverse allgemein. Hier erhältst du News, findest neue Projekte und bekommst auch eine anfängliche Übersicht.
- [instances.social](https://instances.social): Bietet eine umfangreiche Liste mit Mastodon-Instanzen. Filterbar nach Themen, aber kein Anspruch auf Vollständigkeit.
- [Fediverse Space](fediverse.space): Eine Visualisierung, welche Instanzen viel miteinander teilen.
- [search.social](search.social): Eine Suchmaschine für das Fediverse. Hier kannst du nach deinen Lieblingsthemen suchen und Beiträge finden.
- [WeDistribute.org](wedistribute.org): Fediverse Blog
- [Fediverse.Network](https://fediverse.network/): Das Gleiche in grün. Hier nur allgemein für alle föderierten Netzwerke.
- [the Federation Info](https://the-federation.info): Eine Seite gefüllt mit Statistiken zum Fediverse.
- [Blogbeitrag auf kaptain.info](https://kaptain.info/article/7-deutschsprachige-mastodon-instanzen/): Eine Liste deutschsprachiger Mastodon-Instanzen. Zeigt Infos zu Betreiber_innen, Themen und Möglichkeiten der Registrierung. Natürlich ohne Anspruch auf Vollständigkeit. 
